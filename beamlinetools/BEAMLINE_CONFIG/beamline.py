from bessyii_devices.ring import Ring
from bessyii_devices.au import AU1UE52SGM
from bessyii_devices.pinhole import PinholeUE52SGM
#from bessyii_devices.m3_m4_mirrors import SMUMetrixs
from bessyii_devices.m3_m4_mirrors import SMUUE52SGM
from bessyii_devices.undulator import UndulatorUE52
from bessyii_devices.pgm import SGMUE52
from bessyii_devices.keithley import Keithley6517, Keithley6514

#from bessyii_devices.lock import Lock

from ophyd.sim import noisy_det,det1, det2, det3, det4, motor, motor1, motor2, motor, det   # two simulated detectors


#Beamline Lock
#lock = Lock("UE52SGMEL:LOCK00:BEAMLINE:",name="lock")

# print('\n\n########################################')
# print("Connecting to Devices")

# Ring
bessy2 = Ring('MDIZ3T5G:', name='ring')

# Keithleys
kth01 = Keithley6514('AXSYS-TES:' + 'Keithley01:',  name='Keithley01',read_attrs=['readback'])
# kth01.wait_for_connection()
kth02 = Keithley6514('AXSYS-TES:' + 'Keithley02:',  name='Keithley02',read_attrs=['readback'])
# kth02.wait_for_connection()

# sgm
sgm = SGMUE52('ue521sgm1:', name='sgm')

# Undulators
ue52 = UndulatorUE52('UE52ID5R:', name='ue52')

# Apertures
au1 = AU1UE52SGM('OMSX_:h090000', name = 'au1')

# Pinhole
ph = PinholeUE52SGM('OMSX_:h090100', name = 'ph')

# Mirrors

m1 = SMUUE52SGM('SMUYU109L:', name='m1')
