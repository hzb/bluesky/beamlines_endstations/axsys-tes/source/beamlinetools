from bluesky.preprocessors import SupplementalData
from .base import *
from .beamline import *

from bessyii.default_detectors import SupplementalDataSilentDets, init_silent, close_silent

# here all the elements that should be present in the baseline
# separated by a "",""
# baseline = [bessy2.current, bessy2.lifetime,
#               ue52.id_control, ue52.gap, ue52.harmonic_01_eV, ue52.shift,
#               au1.top, au1.bottom, au1.right, au1.left,
#               m1.tx, m1.rx, m1.ry, m1.rz,
#               sgm.en, sgm.diff_order, sgm.cff, sgm.grating, sgm.slitwidth,
#               ph.h,ph.v
#               ]

#########################################################
#
#           new 02.11.2023
#
#########################################################

#initiate SupplementalData
sd = SupplementalDataSilentDets(baseline=[det1,det2], silent_devices=[det1, det2])

#add the functions to the RunEngine library so you can call them via Msg
RE.register_command('init_silent', init_silent)
RE.register_command('close_silent', close_silent)
RE.preprocessors.append(sd)
