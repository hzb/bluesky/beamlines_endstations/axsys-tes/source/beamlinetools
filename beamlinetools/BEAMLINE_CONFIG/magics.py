from bessyii.magics.simplify_syntax import Simplify
from bessyii.magics.peakinfo import PeakInfoMagic
from IPython import get_ipython

# standard magics
from bluesky.magics import BlueskyMagics
get_ipython().register_magics(BlueskyMagics)

simplify = Simplify(get_ipython())
simplify.autogenerate_magics('/opt/bluesky/beamlinetools/beamlinetools/BEAMLINE_CONFIG/plans.py')

get_ipython().register_magics(PeakInfoMagic)
# usage: peakinfo
