import time

from bluesky.plan_stubs import mv, abs_set
from bluesky.plans import count,scan, list_grid_scan, grid_scan

from IPython import get_ipython
user_ns = get_ipython().user_ns


# the following functons are left here because they might 
# be useful. They must be edited depending on the beamline components names


###########################################################################
# IDon and IDoff

def IDon():
    user_ns["sgm"].ID_on.put(1) # 1 should set IDon
    user_ns["ue52"].id_control.put(1) # 1 should set to remote

def IDoff():
    user_ns["sgm"].ID_on.put(0) # 1 should set IDoff
    user_ns["ue52"].id_control.put(0) # 1 should set to local
    
    
###########################################################################
# Set Harmonic

def SetHarmonic(Harmonic):
    """
    Change the harmonic for the UE48 undulator
    
    Parameters
    ----------
    harmonic : int or str
        1 for the first harmonic
        3 for the second harmonic
        5 for the fifth harmonic
        7 for the seventh harmonic
        'maxflux' for maxflux
        'auto' for auto
        
    """


        
    d={1:0,3:1,5:2,7:3,'maxflux':4, 'auto':5}    
    if Harmonic not in d:
        print("Value not permitted.")
        print("Permitted values are", d.keys())
    else:
        user_ns["sgm"].harmonic.put(d[Harmonic])
    
###########################################################################
# Set Table and Harmonic

def SetTable(table):
    """
    Change the table for the UE48 undulator
    
    Parameters
    ----------
    table : int or str 
        "1200_pos_h3", "1200_neg_h3", "1200_hor_h3", "1200_vert_h3",
        "Mn_pos_cont", "Mn_neg_cont",
        "Lin15deg", "Lin30deg", "Lin45deg", "Lin60deg", "Lin75deg",
        "Fe_pos_cont", "Fe_neg_cont",
        "900_hor_h1", "900_pos", "900_neg"
        or an integer between [0,11],
        see BluePanels for the options
    """

    d={"1200_pos_h3":0,"1200_neg_h3":1,"1200_hor_h3":2,"1200_vert_h3":3,
       "Mn_pos_cont":4,"Mn_neg_cont":5,
       "Lin15deg":6,"Lin30deg":7,"Lin45deg":8,"Lin60deg":9,"Lin75deg":10,
       "Fe_pos_cont":11,"Fe_neg_cont":12,
       "900_hor_h1":13, "900_pos":14, "900_neg":15 }
    if table in d:
        user_ns["sgm"].table.put(d[table])
    elif isinstance(table,int)==True:
        if table >=0 or table <= 11:
            user_ns["sgm"].table.put(d[table])
        else:
            print("Permitted Values are:")
            print("str: ", d.keys())
            print("int: values between [0,11]")





    

