import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FixedLocator, FormatStrFormatter
import numpy as np
import warnings

from lmfit.models import LinearModel, SkewedVoigtModel, Model
from lmfit import Parameters

from numpy import (arctan, copysign, cos, exp, isclose, isnan, log, pi, real,
                   sin, sqrt, where)
from scipy.special import erf, erfc
from scipy.special import gamma as gamfcn
from scipy.special import wofz


from IPython import get_ipython


class UndulatorFit:
    """
    A class to fit the undulator spectra and return the RP and the 
    1st valley over 3rd peak ratio
    
    instantiate with 
      from .base import *
      from beamlinetools.undulator_fit import UndulatorFit
      ufit = UndulatorFit(db)
      fit_undulator = ufit.fit_undulator


    then use with:
    
      fit_undulator(identifier,...)

    """
    def __init__(self, db):
        self._db = db
        #some useful variables for defining the voigt functions
        self.tiny = 1.0e-15
        self.s2   = sqrt(2)
        self.s2pi = sqrt(2*pi)
        
    def retrieve_spectra(self, identifier, motor=None, detector=None):
        """
        Retrieve the motor and detector values from one scan

        Parameters
        ----------
        identifier : negative int or string
            for the last scan -1
            or use the db indentifier
        motor : string
            the motor and axis name connected by a _
            for instance m1.tx would be m1_tx
        detector : string
            the detector to retrieve, if more than one detector was used
            in the scan and we don't want to use the first one

        Return
        --------
        x,y : np.array
            two arrays containing motor and detector values
        """
        run       = self._db[identifier]
        if detector == None:
            detector  = run.metadata['start']['detectors'][0]
        if motor == None:
            motor = run.metadata['start']['motors'][0]
        spectra   = run.primary.read()
        x    = np.array(spectra[motor])
        y = np.array(spectra[detector])

        x,y = self.remove_neg_values(x,y)
        return x, y
    def retrieve_scan_identifier(self,identifier):
        """
        Retrieve scan identifier from one scan

        Parameters
        ----------
        identifier : negative int or string
            for the last scan -1
            or use the db indentifier
            'Jens': if available it loads the data from P04,
            the old beamline of J.Viefhaus

        Return
        --------
        bluesky_id : string
            bluesky identifier
        spec_number: string
            spec scan number
        """
        run = self._db[identifier]
        self.spec_number = str(run.metadata['start']['scan_id'])
        self.bluesky_id  = run.metadata['start']['uid']
        return self.bluesky_id, self.spec_number
    def remove_neg_values(self, x,y):
        """
        Remove negative values from y and corresponding values from x

        Parameters
        ----------
        x : numpy.array
        y : numpy.array

        Return
        --------
        x,y : np.array
            two arrays without negative values
        """
        ind = np.where(y < 0)
        x = np.delete(x,ind[0])
        y = np.delete(y,ind[0])
        return x,y

    def RMS(self, data_):
        """
        Calculates Root Mean Square from residuals of a fit

        Parameters
        ----------
        data_ : numpy.array
             residuals


        Return
        --------
        rms : np.array
        """
        sum_data = 0
        for i in data_:
            sum_data = sum_data + i**2          
        rms = np.sqrt((1/len(data_))*sum_data)
        return rms

    def find_nearest_idx(self, array, value):
        """
        Find the index of the values in array closer to value

        Parameters
        ----------
        array : numpy.array
        value : int or float     


        Return
        --------
        idx : int
        """
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return idx

    def guess_amp(self, x,y,vc):
        """
        Guess the amplitude for to input in skewd voigt model starting from
        real data x and y, at the value of x closer to vc

        Parameters
        ----------
        x : numpy.array
        y : numpy.array
        vc : int or float     


        Return
        --------
        amp : float
        """
        idx = self.find_nearest_idx(x,vc)
        amp = y[idx]
        return amp

    def find_max(self, x,y,fwhm):
        """
        Finds the max in a spectra. 

        Parameters
        ----------
        x : numpy.array
        y : numpy.array
        fwhm : float
             the fwhm of the nitrogen peak, used to decide how
             wide the scan window is

        Return
        --------
        idy : int
             the position of the first max in x
        """
        argmax = np.argmax(y)          
        return x[argmax]

    


    ##########################
    # functions specific to fix params fit_data

    # tiny had been numpy.finfo(numpy.float64).eps ~=2.2e16.
    # here, we explicitly set it to 1.e-15 == numpy.finfo(numpy.float64).resolution

    def voigt(self, x, amplitude=1.0, center=0.0, sigma=1.0, gamma=None):
        """Return a 1-dimensional Voigt function.
        voigt(x, amplitude, center, sigma, gamma) =
            amplitude*wofz(z).real / (sigma*s2pi)
        For more information, see: https://en.wikipedia.org/wiki/Voigt_profile
        """
        if gamma is None:
            gamma = sigma
        z = (x-center + 1j*gamma) / max(self.tiny, (sigma*self.s2))
        return amplitude*wofz(z).real / max(self.tiny, (sigma*self.s2pi))

    def skewed_voigt(self, x, amplitude=1.0, center=0.0, sigma=1.0, gamma=None, skew=0.0):
        """Return a Voigt lineshape, skewed with error function.
        Equal to: voigt(x, center, sigma, gamma)*(1+erf(beta*(x-center)))
        where ``beta = skew/(sigma*sqrt(2))``
        with ``skew < 0``: tail to low value of centroid
             ``skew > 0``: tail to high value of centroid
        Useful, for example, for ad-hoc Compton scatter profile. For more
        information, see: https://en.wikipedia.org/wiki/Skew_normal_distribution
        """
        beta = skew/max(self.tiny, (self.s2*sigma))
        asym = 1 + erf(beta*(x-center))
        return asym * self.voigt(x, amplitude, center, sigma, gamma=gamma)

    def undulator_model(self, x, a1, c1, sigma,gamma,skew):
        tw = self.skewed_voigt(x,amplitude=a1,  center=c1,  sigma=sigma,  gamma=gamma,  skew=skew) 
        return tw

    # internal fit routine
    def _fit_undulator(self, x,y, print_fit_results=False, save_img=False,fit_data=True, 
                c1='auto', amp_sf=6,sigma = 0.02, sigma_min=0.001,sigma_max=0.02,gamma=0.055):
        norm = np.max(y)
        y = y/norm

        # boundaries for the fit
        amp_min   = 0
        amp_mf    = 1.1 
        gamma_min = 0

        fwhm      = 2.355*sigma*2 # this depends in the model
        
        
        if c1 == 'auto':
            c1 = self.find_max(x,y, fwhm)

        

        print('Using fixed parameters fit')
        guess = {'c1': c1, 'amp1':np.max(y)/amp_sf}

        pars = Parameters()


        mod = Model(self.undulator_model) 
        pars = mod.make_params(a1=guess['amp1'],
                        c1=guess['c1'],
                        sigma=sigma,gamma=gamma,skew=0)

        #################################################################################
        ################################################################################
        # 
        init = mod.eval(pars, x=x)


        # here it plots the intial guesses if fit=False and returns
        if fit_data == False:
            plt.rc("font", size=12,family='serif')
            fig, axes = plt.subplots(1, 1, figsize=(8.0, 16.0))
            axes.plot(x, init, 'orange' ,label='initial guess')
            axes.scatter(x, y, label='data')
            #axes.set_ylim(-.05,1.1)
            return None,None,None,None,None

        # fit is performed here
        out = mod.fit(y, pars, x=x)
        delta = out.eval_uncertainty(x=x)
        if print_fit_results == True:
            print(out.fit_report(min_correl=0.5))

        # plot results
        # axes 0, plot data, initial guess and fit
        plt.rc("font", size=12,family='serif')
        fig, axes = plt.subplots(2, 1, figsize=(8.0, 16.0))
        plt.suptitle('Scan n:'+self.spec_number+' uid:'+self.bluesky_id)
        # initial fit
        axes[0].plot(x, out.init_fit, 'orange' ,label='initial guess')
        #
        axes[0].scatter(x, y, label='data', s=10)
        axes[0].plot(x, out.best_fit, 'r', label='best fit')
        axes[0].fill_between(x,out.best_fit-delta,out.best_fit+delta,color='gray',alpha=0.4, label='fit uncertainty')


        axes[0].legend()
        axes[0].set_xlabel('Photon energy (eV)')
        axes[0].set_ylabel('Intensity [a.u.]')

        axes[0].set_ylim(np.min(y)-0.25,np.max(y)+0.25)
        axes[0].tick_params(axis='both',direction='in',length=6,top=True,right=True)

        # set tick spacing
        XminorLocator = MultipleLocator(0.1)
        YminorLocator = MultipleLocator(100000)
        axes[0].xaxis.set_minor_locator(XminorLocator)
        axes[0].yaxis.set_minor_locator(YminorLocator)
        axes[0].tick_params(which='minor',axis='both',direction='in',length=3,top=True,right=True)
        #

        #
        ################################################################################
        # axes 1, plot RMS

        residuals_inital = y - out.init_fit
        residuals_final = y - out.best_fit
        #
        axes[1].plot(x, residuals_final, label='data-best fit')
        axes[1].fill_between(x,np.sqrt(y)*-3.0,np.sqrt(y)*3.0,color='gray',alpha=0.4, label='sqrt(data)*\u00b13')
        axes[1].legend()
        axes[1].set_xlabel('Photon energy (eV)')
        axes[1].set_ylabel('Residual counts (arb. units)')
        axes[1].tick_params(axis='both',direction='in',length=6,top=True,right=True)

        XminorLocator = MultipleLocator(0.1)
        YminorLocator = MultipleLocator(1000)
        axes[1].xaxis.set_minor_locator(XminorLocator)
        axes[1].yaxis.set_minor_locator(YminorLocator)
        axes[1].tick_params(which='minor',axis='both',direction='in',length=3,top=True,right=True)
        # 
        xmin_ax2, xmax_ax2 = axes[1].axes.get_xlim()
        ymin_ax2, ymax_ax2 = axes[1].axes.get_ylim()

        axes[1].text(((xmax_ax2-xmin_ax2)*0.55)+xmin_ax2, ((ymax_ax2-ymin_ax2)*0.25)+ymin_ax2, f'RMS initial = {str(np.round(self.RMS(residuals_inital),4))}') #, style = 'italic',fontsize = 30,  color = "green")
        axes[1].text(((xmax_ax2-xmin_ax2)*0.55)+xmin_ax2, ((ymax_ax2-ymin_ax2)*0.15)+ymin_ax2, f'RMS final   = {str(np.round(self.RMS(residuals_final),4))}')

        plt.subplots_adjust(left=0.14, bottom=None, right=None, top=None, wspace=0.04, hspace=0.14)
        if save_img != False:
            plt.savefig(save_img+'.pdf')
        plt.show()


        return 

    def fit_undulator(self, scan, motor='sgm.en', detector='Keithley01',
                      print_fit_report=False,save_img=False, fit=True,
                      c1='auto', amp_sf=6,
                      sigma = 0.02, sigma_min=0.001,sigma_max=np.inf,
                      gamma=0.0563):
        """
        This function calls _fit_undulator to perform a fit on the array x and y of a undulator spectra. 
        The initial guess of the fit can be modified via the arguments.

        Parameters
        ----------
        scan : negative int or string
            for the last scan -1
            or use the db indentifier
        motor : string
            the motor and axis name connected by a _
            for instance m1.tx would be m1_tx
        detector : string
            the detector to retrieve, if more than one detector was used
            in the scan and we don't want to use the first one
        print_fit_results: boolean
            if True the fit results report from lm fit is printed on the terminal
        save_img: boolean or string
            if False no image is saved
            if is a string use the image will be saved with this name 
            (include the extension: pdf,png,jpg...)
        fit_data: boolean
            If False the data and the initial guess will be shown
            If True the fit will be performed
        c1: string or float:
            If 'auto' the first max will be guessed automatically
            if the automatic guess fails input the position of the first max
        amp_sf: int or float
            scaling factor for the amplitude. empirically a value of 6 works well
        sigma: int or float
            the sigma of the gaussian contribution to the voigt peak   
        sigma_min: int or float
            the lower bound for the sigma parameter for the fit
        sigma_max: int or float
            the upper bound for the sigma parameter for the fit
        gamma: int or float
            the gamma of the loretzian contribution to the voigt peak

        """
        energy, intensity = self.retrieve_spectra(scan)
        _,_               = self.retrieve_scan_identifier(scan)
        self._fit_undulator(energy, intensity, 
                            print_fit_results=print_fit_report, save_img=save_img,fit_data=fit,
                            c1=c1,amp_sf=amp_sf,
                            sigma=sigma,sigma_min=sigma_min,sigma_max=sigma_max,
                            gamma=gamma)


        return 




